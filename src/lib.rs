pub trait Smell {
    fn smell(&self) -> bool;
}
