extern crate orphan_trait_demo;

use std::vec::Vec;

use orphan_trait_demo::Smell;

impl<T: Smell> Smell for Vec<T> {
    fn smell(&self) -> bool {
        self.into_iter().fold(false, |acc, x| acc || x.smell())
    }
}

#[derive(Clone, Copy, Default)]
struct Foot {}

impl Smell for Foot {
    fn smell(&self) -> bool {
        true
    }
}

fn main() {
    let v = vec![Foot {}; 2];
    if v.smell() {
        println!("Yes");
    } else {
        println!("no");
    };
}
